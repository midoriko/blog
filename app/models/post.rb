class Post < ActiveRecord::Base
has_many :comments, dependent: :destroy #[has_many: comments] establish the relationship btw the post and the comments [destroy] destroys all the comments that are dependable of the        correspondent post #
validates_presence_of :title
validates_presence_of :body
end
